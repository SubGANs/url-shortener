from flask import Flask, request, render_template, redirect
import math
from urllib.parse import urlparse
import sqlite3

HOST = ""

BASE = 62

UPPERCASE_OFFSET = 55
LOWERCASE_OFFSET = 61
DIGIT_OFFSET = 48


def true_ord(char):
    if char.isdigit():
        return ord(char) - DIGIT_OFFSET
    elif 'A' <= char <= 'Z':
        return ord(char) - UPPERCASE_OFFSET
    elif 'a' <= char <= 'z':
        return ord(char) - LOWERCASE_OFFSET
    else:
        raise ValueError("%s is not a valid character" % char)


def true_chr(integer):
    if integer < 10:
        return chr(integer + DIGIT_OFFSET)
    elif 10 <= integer <= 35:
        return chr(integer + UPPERCASE_OFFSET)
    elif 36 <= integer < 62:
        return chr(integer + LOWERCASE_OFFSET)
    else:
        raise ValueError("%d is not a valid integer in the range of base %d" % (integer, BASE))


def encode(key):
    if key == 0:
        return '0'

    string = ""
    while key > 0:
        remainder = key % BASE
        string = true_chr(remainder) + string
        key = int(key / BASE)
    return string


def decode(key):
    int_sum = 0
    reversed_key = key[::-1]
    for idx, char in enumerate(reversed_key):
        int_sum += true_ord(char) * int(math.pow(BASE, idx))
    return int_sum


app = Flask(__name__)


def table_check():
    create_table = """
        CREATE TABLE WEB_URL(
        ID INTEGER PRIMARY KEY AUTOINCREMENT,
        URL  TEXT NOT NULL UNIQUE);
        """
    with sqlite3.connect('urls.db') as conn:
        cursor = conn.cursor()
        try:
            cursor.execute(create_table)
            conn.commit()
        except sqlite3.OperationalError:
            pass


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        original_url = request.form.get('url')
        if urlparse(original_url).scheme == '':
            original_url = 'http://' + original_url
        if 'url.subgans.ru' in original_url:
            return render_template('index.html', short_url=original_url)
        with sqlite3.connect('urls.db') as conn:
            cursor = conn.cursor()
            sql = "INSERT OR IGNORE INTO WEB_URL (URL) VALUES (?)"
            result_cursor = cursor.execute(sql, (original_url,))
            conn.commit()
            if result_cursor.lastrowid == 0:
                sql = "SELECT ID FROM WEB_URL WHERE URL=?"
                cursor.execute(sql, (original_url,))
                encoded_string = encode(cursor.fetchone()[0])
            else:
                encoded_string = encode(result_cursor.lastrowid)

        if request.form.get('api'):
            return HOST + "/" + encoded_string
        return render_template('index.html', short_url=HOST + "/" + encoded_string)
    return render_template('index.html')


@app.route('/<short_url>')
def redirect_short_url(short_url):
    try:
        decoded_string = decode(short_url)
    except:
        return render_template('index.html')
    redirect_url = HOST
    with sqlite3.connect('urls.db') as conn:
        cursor = conn.cursor()
        sql = "SELECT URL FROM WEB_URL WHERE ID=?"
        cursor.execute(sql, (decoded_string,))
        try:
            redirect_url = cursor.fetchone()[0]
        except Exception as e:
            print(e)
    return redirect(redirect_url)


if __name__ == '__main__':
    table_check()
    app.run(debug=False)
